import sys
import re
import os

DIR = "_testsplit_%s" % sys.argv[1]
if not os.path.exists(DIR):
    os.makedirs(DIR)

fullClass = list()


for line in open(sys.argv[1]).readlines():
    fullClass.append(line.replace("\n", ""))


header = list()
headerComplete = False

tests = list()
currentTest = list()
inTest = False
bracePassed = False
braceCount = 0

footer = list()



def processTest(test):
    testLines = list()
    
    # Get test name
    name = ""
    for line in test:
        if ("public void" in line):
            # TODO use regex instead
            name = line.replace("public void ", "").replace("()", "").replace(" ", "").replace("\n", "").replace("\t", "").replace("{", "")
            # Capitalize
            name = re.sub('([a-zA-Z])', lambda x: x.groups()[0].upper(), name, 1)
            break

    # Add header 
    for line in header:
        # Replace class definition with test name
        if ("public class" in line):
            # TODO fix
            testLines.append(re.sub(
                r"public class .+\b",
                "public class %s " % name,
                line
            ))
        else:
            testLines.append(line)

    for line in test:
        testLines.append(line)
    
    for line in footer:
        testLines.append(line)

    # Store
    with open(DIR + "/" + name + ".java", "w") as f:
        for line in testLines:
            f.write("%s\n" % line)
    


for line in fullClass:

    # Header
    if (not headerComplete):
        if ("@Test" in line):
            headerComplete = True
        else:
            header.append(line)
            continue

    # @Test start
    if ("@Test" in line):
        if(inTest):
            # New test, store the existing one, then clear currentTest
            tests.append(currentTest)
            currentTest = list()
            currentTest.append(line)
            continue
        else:
            inTest = True
            currentTest.append(line)
            continue

    # In Test
    if(inTest):
        currentTest.append(line)
        # Track number of braces
        if (not bracePassed):
            if ("{" in line):
                bracePassed = True
        
        if (bracePassed):
            braceCount += line.count("{")
            braceCount -= line.count("}")

        # Test ended
        if (bracePassed and braceCount == 0):
            inTest = False
            bracePassed = False
            tests.append(currentTest)
            currentTest = list()
            
        continue

    # Footer (or other non-test, non-header content)
    if(not inTest):
        footer.append(line)
        continue



    
for test in tests:
    processTest(test)
    print("======================")
